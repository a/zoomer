package main

import ("fmt"
        "os"
        "bytes"
        "io/ioutil"
        "net/http"
        "encoding/json"
        "github.com/buger/jsonparser"
)

var subStates map[string]string
var informUrls []byte
var httpClient *http.Client

func checkErr(err error) {
    if err != nil {
        panic(err)
    }
}

func checkSubState(sub string) string {
    // Prepare the URL
    url := "https://old.reddit.com/r/" + sub + ".json"

    // Prepare request, set U-A so that we don't get ratelimited
    req, err := http.NewRequest("GET", url, nil)
    checkErr(err)
    req.Header.Add("User-Agent", "subreddit status checker by /u/aveao")

    // Do the request we prepared, close body as recommended by official sources
    // (The client must close the response body when finished with it)
    resp, err := httpClient.Do(req)
    checkErr(err)
    defer resp.Body.Close()

    // Read body, gives a byte array
    body, err := ioutil.ReadAll(resp.Body)
    checkErr(err)

    // Grab and return the "reason" field from json
    // TODO: get rid of jsonparser here, go for go's json parser
    if subState, _, _, _ := jsonparser.Get(body, "reason"); len(subState) != 0 {
        return string(subState)
    }
    return "alive"
}

func informDiscord(sub string, state string) {
    // Check various states, act accordingly
    if state == "private" {
        return
    } else if state == "alive" {
        return
    } else if state == "banned" {
        state = "gone"
    }

    text := "🦀🌍 /r/" + sub + " is " + state + " 🌍🦀"

    // Create webhook contents
    webhookEntry := map[string]string{"content": text}
    webhookJson, err := json.Marshal(webhookEntry)
    checkErr(err)

    // Get list of webhooks
    var informUrlsStr []string
    err = json.Unmarshal(informUrls, &informUrlsStr)
    checkErr(err)

    // Send good news
    for _, entry := range informUrlsStr {
        informUrl := string(entry)
        fmt.Println(text)
        _, err = http.Post(informUrl, "application/json", bytes.NewBuffer(webhookJson))
        checkErr(err)
    }
}

func compareSubState(sub string) {
    savedState := subStates[sub]

    // Skip over subs we know are banned
    if savedState == "banned" {
        return
    }

    // Check a sub's state, log it to stdout, compare to the saved state
    subState := checkSubState(sub)
    fmt.Println(sub + ": " + subState + " (local: " + savedState + ")")
    if subState != savedState {
        // Update state, and inform discord about the good news
        subStates[sub] = subState
        informDiscord(sub, subState)
    }
}

func loadStates() {
    state, err := ioutil.ReadFile("zoomerstate.json")
    checkErr(err)

    err = json.Unmarshal(state, &subStates)
    checkErr(err)
}

func saveStates() {
    stateJson, err := json.Marshal(subStates)
    checkErr(err)

    err = ioutil.WriteFile("zoomerstate.json", stateJson, 0644)
    checkErr(err)
}

func main() {
    configFile, err := os.Open("zoomer.json")
    checkErr(err)
    defer configFile.Close()

    config, err := ioutil.ReadAll(configFile)
    checkErr(err)

    // Prepare http client
    httpClient = &http.Client{}

    // TODO: get rid of jsonparser here, go for go's json parser
    informUrls, _, _, err = jsonparser.Get(config, "informUrls")
    checkErr(err)

    loadStates()

    // TODO: get rid of jsonparser here, go for go's json parser
    jsonparser.ArrayEach(config, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
        sub := string(value)
        compareSubState(sub)
    }, "trackedsubreddits")

    saveStates()
}
